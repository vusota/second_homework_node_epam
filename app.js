const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 8080;

const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');
const userRouter = require('./routers/userRouter');

app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/notes', noteRouter);
app.use('/api/users/me', userRouter);
app.use((err, req, res) => {
  res.status(500).json({message: err.message});
});

const init = async () => {
  await mongoose.connect(
      'mongodb+srv://taskapp:taskapp@cluster0.sd3er.mongodb.net/task-api?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
  );

  app.listen(port, () => {
    console.log(`Port is: ${port}!`);
  });
};

init();
