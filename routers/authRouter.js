const express = require('express');
const router = new express.Router();
const {validateRegistration} = require('../registration/validateRegistration');
const {registration, login} = require('../registration/registration');

const asyncWrapper = (callback) => {
  return (req, res, next) => {
    callback(req, res, next).catch(next);
  };
};

router.post(
    '/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(registration),
);
router.post('/login', asyncWrapper(login));

module.exports = router;
